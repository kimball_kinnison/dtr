"""
analysis of data from completed renders

callable functions from this file:

    display_basic_render_info

------------------------------------------------------------------------------
Copyright 2015-2017 Alan Taylor

This file is part of dtr.

dtr is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

dtr is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with dtr.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------------
"""

import datetime                 # timedelta


##############################################################################
# print information for user
##############################################################################

def display_basic_render_info(render_nodes, start, end, time_taken_previous_renders):
    """
    display the number of blocks each node completed, and the mean block
    render time

    --------------------------------------------------------------------------
    args
        render_nodes : list of RenderNode instances
            contains information about all the nodes in the cluster
        start : float
            script start time
        end : float
            script start time
        time_taken_previous_renders : float
            this will be zero if the render has not been restarted or
            will represent the accumulated time of previous runs of the
            script for this distributed render
    --------------------------------------------------------------------------
    returns : none
    --------------------------------------------------------------------------
    """
    total_time = end - start + time_taken_previous_renders
    print('\n' + 'node'.rjust(16) + 'blocks'.rjust(8) + 'mean duration'.rjust(20))

    render_nodes.sort(key=lambda x: x.num_blocks, reverse=True)
    for i in render_nodes:
        print(i.ip_address.rjust(16) + str(i.num_blocks).rjust(8) + \
            str(datetime.timedelta(seconds=int(i.mean_duration))).rjust(20))
    print('\n' + 'total time'.rjust(44) + '\n' + \
        str(datetime.timedelta(seconds=int(total_time))).rjust(44))
